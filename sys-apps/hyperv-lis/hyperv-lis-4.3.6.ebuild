# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit linux-info

DESCRIPTION="Linux Integration Services for Hyper-V virtual machines"
HOMEPAGE="https://docs.microsoft.com/en-us/windows-server/virtualization/hyper-v/supported-linux-and-freebsd-virtual-machines-for-hyper-v-on-windows"
SRC_URI=""
SLOT="0"
KEYWORDS="amd64 x86"

FCOPY_PN="hv_fcopy_daemon"
KVP_PN="hv_kvp_daemon"
VSS_PN="hv_vss_daemon"
CONFIG_CHECK="~HYPERV_UTILS"
ERROR_HYPERV_UTILS="CONFIG_HYPERV_UTILS is not enabled. VSS daemon will not interact with the kernel."

pkg_setup() {
	if ! kernel_is 5 ; then
		eerror "This version of LIS is designed to build against the version 5 tree of the kernel sources."
		eerror "Please install the proper LIS version for your kernel."
		die
	fi
}

src_unpack() {
	# C files
	mkdir -p "${S}"/src
	cp "${KERNEL_DIR}"/tools/hv/*fcopy*.c "${S}/src"
	cp "${KERNEL_DIR}"/tools/hv/*kvp*.c "${S}/src"
	cp "${KERNEL_DIR}"/tools/hv/*vss*.c "${S}/src"

	# scripts
	mkdir -p "${S}"/scripts
	cp "${KERNEL_DIR}"/tools/hv/*.sh "${S}/scripts"

	# headers
#	mkdir -p "${S}"/include/uapi/linux/
	mkdir -p "${S}"/include/linux/
#	cp "${KERNEL_DIR}"/include/uapi/linux/hyperv.h "${S}"/include/uapi/linux
#	cp "${KERNEL_DIR}"/include/uapi/linux/connector.h "${S}"/include/uapi/linux
	cp "${KERNEL_DIR}"/include/uapi/linux/hyperv.h "${S}"/include/linux
	cp "${KERNEL_DIR}"/include/uapi/linux/connector.h "${S}"/include/linux
}

src_prepare() {
	# change directories
	sed -r -i 's/\/var\/opt/\/var\/lib/g' "${S}"/src/*.c

	# change scripts
	sed -r -i 's/(hv_(get_(dns|dhcp)_info|set_ifconfig))/\/etc\/hyperv\/\1.sh/g' "${S}"/src/*.c
	eapply_user
}

src_compile() {
	# compile FCOPY daemon
	gcc -I"${S}"/include "${S}"/src/*fcopy*.c -o "${S}"/src/${FCOPY_PN}

	# compile KVP daemon
	gcc -I"${S}"/include "${S}"/src/*kvp*.c -o "${S}"/src/${KVP_PN}

	# compile VSS daemon
	gcc -I"${S}"/include "${S}"/src/*vss*.c -o "${S}"/src/${VSS_PN}
}

src_install() {
	dosbin src/${FCOPY_PN}
	dosbin src/${KVP_PN}
	dosbin src/${VSS_PN}

	exeinto /etc/hyperv
	doexe "${S}"/scripts/*

	newinitd "${FILESDIR}"/${PN}.initd ${PN}
}

